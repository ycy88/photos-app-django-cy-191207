1 Post Photo

- POST /photos/ (needs token, pass "caption" and "image")

2 Save Photo as draft

- POST /drafts/ (needs token to save draft)

3 Edit photo captions

- PATCH /photos/:id
- (needs token pass new "caption", can only update user's photos) (note use PATCH instead of PUT to allow partial updates)

4 Delete photos

- DELETE/photos/:id
- (needs token, can only delete user's photos)

5A List photos

- GET /photos/ (can see published photos by all users)

5B List my photos

- GET /user/photos/ (needs token to see only my photos)

5C List my drafts

- GET /drafts/ (needs token to see only my drafts)

6 ASC/DESC sort photos

- GET /photos/?ordering=published_at or GET photos/?ordering=-published_at

7 Filter all photos by user

- GET /photos/?user=1

8 Image limits

- Image width must be less than 1000, height must be less than 1000
- Image must not be bigger than 1 MB

9 JWT Authentication

- POST /api/token/ (pass "username" and "password")

LIMITATIONS

- Access Token expires in one hour. Need to fetch a new one after that.

- Heroku will throw H18 "Server request interrupted" error if the request does not conform to the format (e.g. sending an extra body along with a DELETE request). This returns a HTML page with application fail message.
