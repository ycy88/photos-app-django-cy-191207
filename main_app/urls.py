from rest_framework.urlpatterns import format_suffix_patterns
from main_app import views
from django.urls import path

urlpatterns = [
    path('photos/', views.PhotoList.as_view()),
    path('photos/<int:pk>/', views.PhotoDetail.as_view()),
    path('drafts/', views.DraftList.as_view()),
    path('user/photos/', views.UserPhotoList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
