from django.db import models
from django.contrib.auth.models import User
from django.core.validators import validate_image_file_extension
from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError
import uuid


def validate_dimensions(image):
    # MIN_WIDTH = 400
    MAX_WIDTH = 1000
    # MIN_HEIGHT = 400
    MAX_HEIGHT = 1000
    FILE_SIZE_LIMIT_MB = 1

    width, height = get_image_dimensions(image)
    filesize = image.size

    if filesize > FILE_SIZE_LIMIT_MB * 1024 * 1024:
        raise ValidationError(
            f"Image must not be bigger than {FILE_SIZE_LIMIT_MB} MB")

    if width > MAX_WIDTH or height > MAX_HEIGHT:
        raise ValidationError(
            f"Image width must be less than {MAX_WIDTH}, height must be less than {MAX_HEIGHT}")


def generate_img_uuid_path(instance, filename):
    id = uuid.uuid4()
    uuid_filename = f"{id}.{filename.split('.')[-1].lower()}"
    return uuid_filename


class Photo(models.Model):
    DRAFTED = 1
    PUBLISHED = 2
    STATUSES = (
        (DRAFTED, 'drafted'),
        (PUBLISHED, 'published')
    )
    # id will be auto created by the model
    created_at = models.DateTimeField(auto_now_add=True)
    caption = models.CharField(max_length=255, blank=True, default='')
    # add 'related_name' to create a backwards relation
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='photos')
    status = models.PositiveSmallIntegerField(choices=STATUSES)
    published_at = models.DateTimeField(null=True)
    image = models.ImageField(
        validators=[validate_image_file_extension, validate_dimensions], upload_to=generate_img_uuid_path)


# Create your models here.
