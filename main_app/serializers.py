from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Photo


# use ModelSerializer as syntactic sugar instead of normal serializers
class PhotoSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True,
        source='user.id'
    )
    # for some reason CurrentUserDefault() does not work

    class Meta:
        model = Photo
        fields = ['id', 'caption', 'status', 'user', 'published_at', 'image']
        # read_only fields not settable by users, set on server
        read_only_fields = ['status', 'published_at']


# TODO: do we have to use HyperlinkedSerializer to get this to work?

# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ['username', 'photos']
