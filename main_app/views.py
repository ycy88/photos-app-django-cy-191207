from django.shortcuts import render
from rest_framework import generics
from .models import Photo
from .serializers import PhotoSerializer
from django.utils import timezone
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import permissions
from .permissions import IsOwnerOrReadOnly

# Create your views here.


class PhotoList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    queryset = Photo.objects.filter(status=Photo.PUBLISHED)
    serializer_class = PhotoSerializer
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ['user']
    ordering_fields = ['published_at']

    # note: doing it this way bypasses serializer validation
    def perform_create(self, serializer):
        serializer.save(published_at=timezone.now(),
                        status=Photo.PUBLISHED, user=self.request.user)
        # TODO: can do some file magic re-sizing here before saving


class PhotoDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    queryset = Photo.objects.filter(status=Photo.PUBLISHED)
    serializer_class = PhotoSerializer


class DraftList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated]

    serializer_class = PhotoSerializer

    # note: doing it this way bypasses serializer validation
    def perform_create(self, serializer):
        serializer.save(status=Photo.DRAFTED, user=self.request.user)
        # TODO: can do some file magic re-sizing here before saving

    def get_queryset(self):
        user = self.request.user
        return Photo.objects.filter(user=user, status=Photo.DRAFTED)


class UserPhotoList(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]

    serializer_class = PhotoSerializer

    def get_queryset(self):
        user = self.request.user
        return Photo.objects.filter(user=user, status=Photo.PUBLISHED)
